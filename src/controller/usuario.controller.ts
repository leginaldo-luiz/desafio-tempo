import { Body, Controller, Delete, Get, Param, Post, Put, } from '@nestjs/common';
import { UsuarioRepository } from 'src/repository/usuario.repository';
import UsuarioDto from 'src/dto/usuario-dto';

@Controller('usuarios')
export class UsuariosController {
  constructor(private readonly usuarioRepository: UsuarioRepository) {}

  @Get()
  listarUsuarios() {
    return this.usuarioRepository.listarUsuarios();
  }

  @Get(':id')
  listarUsuarioPorId(@Param('id') id: string) {
    return this.usuarioRepository.listarUsuarioPorId(Number(id));
  }

  @Post()
  async cadastrarUsuario(@Body() usuarioDto: UsuarioDto) {
    return this.usuarioRepository.cadastrarUsuario(usuarioDto);
  }

  @Put(':id')
  async editarUsuario(@Param('id') id: string, @Body() usuarioDto: UsuarioDto) {
    return this.usuarioRepository.editarUsuario(Number(id), usuarioDto);
  }

  @Delete(':id')
  async excluirUsuario(@Param('id') id: string) {
    this.usuarioRepository.excluirUsuario(Number(id));
  }
}
