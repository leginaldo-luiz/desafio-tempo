import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
class Usuario {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public nome: string;
}

export default Usuario;
