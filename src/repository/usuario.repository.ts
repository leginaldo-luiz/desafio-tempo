import { HttpException, HttpStatus } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import UsuarioDto from 'src/dto/usuario-dto';
import Usuario from 'src/models/usario.model';

@EntityRepository(Usuario)
export class UsuarioService extends Repository<Usuario> {

  listarUsuarios() {
    return this.find();
  }

  async listarUsuarioPorId(id: number) {
    const usuario = await this.findOne({
      where: {id}
    });
    if (usuario) {
      return usuario;
    }

    throw new HttpException('Usuário não encontrado.', HttpStatus.NOT_FOUND);
  }

  async cadastrarUsuario(usuario: UsuarioDto) {
    const novoUsuario = await this.create(usuario);
    await this.save(novoUsuario);

    return novoUsuario;
  }

  async editarUsuario(id: number, post: UsuarioDto) {
    await this.update(id, post);
    const usuarioAtualizado = await this.findOne({
      where: {id}
    });
    if (usuarioAtualizado) {
      return usuarioAtualizado;
    }

    throw new HttpException('Usuário não encontrado.', HttpStatus.NOT_FOUND);
  }

  async excluirUsuario(id: number) {
    const usuarioExcluido = await this.delete(id);
    if (!usuarioExcluido.affected) {
      throw new HttpException('Usuário não encontrado.', HttpStatus.NOT_FOUND);
    }
  }
}
